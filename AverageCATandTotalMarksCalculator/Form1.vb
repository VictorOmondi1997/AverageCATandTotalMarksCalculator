﻿Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim cat1, cat2, averageCatMark, finalMark, totalMark As Double
        cat1 = Val(txtCat1.Text)
        cat2 = Val(txtCat2.Text)
        finalMark = Val(txtFinal.Text)
        averageCatMark = (cat1 + cat2) / 2
        totalMark = averageCatMark + finalMark
        If (averageCatMark <= 30 And totalMark <= 70) Then
            MsgBox("Your Average Marks is: " & averageCatMark)
            MsgBox("Your Total Marks is: " & totalMark)
        Else
            MsgBox("Cat Should be <=30 and Final Mark <=70")
        End If
    End Sub
End Class
